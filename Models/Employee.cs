using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RPS_URD_Project.Models
{
    public class Employee : Person
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public new string Id { get; set; }
        public Department Department { get; set; }
    }
}