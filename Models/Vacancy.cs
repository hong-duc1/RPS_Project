using System;
using System.Collections.Generic;

namespace RPS_URD_Project.Models
{
    public class Vacancy
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public HREmployee OwnedBy { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int NumberOfJobs { get; set; }
        public Department Department { get; set; }
        public DateTime DateToClose { get; set; }
        public virtual ICollection<ApplicantVacancy> ApplicantVacancies { get; set; }

    }
}