using System;
using System.Collections.Generic;

namespace RPS_URD_Project.Models
{
    public class Applicant : Person
    {
        public DateTime CreateDate { get; set; }
        public string Status { get; set; }
        public virtual ICollection<ApplicantVacancy> ApplicantVacancies { get; set; }
    }
}