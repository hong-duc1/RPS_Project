using System;
using System.Collections.Generic;
namespace RPS_URD_Project.Models
{
    public class ApplicantVacancy
    {
        public int Id { get; set; }
        public Applicant Applicant { get; set; }
        public Vacancy Vacancy { get; set; }
        public DateTime AttachedDate { get; set; }
        public string Status { get; set; }
        public List<Interview> Interviews { get; set; }
    }
}