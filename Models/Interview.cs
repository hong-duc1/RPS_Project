using System;
namespace RPS_URD_Project.Models
{
    public class Interview
    {
        public int Id { get; set; }
        public ApplicantVacancy ApplicantVacancy { get; set; }
        public DateTime InterviewDate { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}