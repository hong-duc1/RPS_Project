using System.ComponentModel.DataAnnotations;

namespace RPS_URD_Project.Models
{
    public class User
    {
        [Key]
        public string Id { get; set; }
        public string Pass { get; set; }
    }
}