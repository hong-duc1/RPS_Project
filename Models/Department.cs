namespace RPS_URD_Project.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}