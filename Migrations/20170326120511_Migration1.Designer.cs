﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using RPS_URD_Project.DAO;

namespace RPS_URD_Project.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20170326120511_Migration1")]
    partial class Migration1
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("RPS_URD_Project.Models.Applicant", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int>("Age");

                    b.Property<string>("City");

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("District");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Phone");

                    b.Property<string>("Picture");

                    b.Property<string>("Status");

                    b.HasKey("Id");

                    b.ToTable("Applicants");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.ApplicantVacancy", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ApplicantId");

                    b.Property<DateTime>("AttachedDate");

                    b.Property<string>("Status");

                    b.Property<int?>("VacancyId");

                    b.HasKey("Id");

                    b.HasIndex("ApplicantId");

                    b.HasIndex("VacancyId");

                    b.ToTable("ApplicantVacancies");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.Department", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Departments");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.HREmployee", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("Address");

                    b.Property<int>("Age");

                    b.Property<string>("City");

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("District");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Phone");

                    b.Property<string>("Picture");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.ToTable("HREmployees");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.Interview", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ApplicantVacancyId");

                    b.Property<DateTime>("EndTime");

                    b.Property<DateTime>("InterviewDate");

                    b.Property<DateTime>("StartTime");

                    b.HasKey("Id");

                    b.HasIndex("ApplicantVacancyId");

                    b.ToTable("Interviews");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.Interviewer", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("Address");

                    b.Property<int>("Age");

                    b.Property<string>("City");

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("District");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Phone");

                    b.Property<string>("Picture");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.ToTable("Interviewers");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Pass");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.Vacancy", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreateDate");

                    b.Property<DateTime>("DateToClose");

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("Description");

                    b.Property<int>("NumberOfJobs");

                    b.Property<string>("OwnedById");

                    b.Property<string>("Status");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("DepartmentId");

                    b.HasIndex("OwnedById");

                    b.ToTable("Vacancies");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.ApplicantVacancy", b =>
                {
                    b.HasOne("RPS_URD_Project.Models.Applicant", "Applicant")
                        .WithMany("ApplicantVacancies")
                        .HasForeignKey("ApplicantId");

                    b.HasOne("RPS_URD_Project.Models.Vacancy", "Vacancy")
                        .WithMany("ApplicantVacancies")
                        .HasForeignKey("VacancyId");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.HREmployee", b =>
                {
                    b.HasOne("RPS_URD_Project.Models.Department", "Department")
                        .WithMany()
                        .HasForeignKey("DepartmentId");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.Interview", b =>
                {
                    b.HasOne("RPS_URD_Project.Models.ApplicantVacancy", "ApplicantVacancy")
                        .WithMany("Interviews")
                        .HasForeignKey("ApplicantVacancyId");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.Interviewer", b =>
                {
                    b.HasOne("RPS_URD_Project.Models.Department", "Department")
                        .WithMany()
                        .HasForeignKey("DepartmentId");
                });

            modelBuilder.Entity("RPS_URD_Project.Models.Vacancy", b =>
                {
                    b.HasOne("RPS_URD_Project.Models.Department", "Department")
                        .WithMany()
                        .HasForeignKey("DepartmentId");

                    b.HasOne("RPS_URD_Project.Models.HREmployee", "OwnedBy")
                        .WithMany()
                        .HasForeignKey("OwnedById");
                });
        }
    }
}
