using Microsoft.EntityFrameworkCore;
using RPS_URD_Project.Models;

namespace RPS_URD_Project.DAO
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<HREmployee> HREmployees { get; set; }
        public DbSet<Interview> Interviews { get; set; }
        public DbSet<Interviewer> Interviewers { get; set; }
        public DbSet<Vacancy> Vacancies { get; set; }
        public DbSet<ApplicantVacancy> ApplicantVacancies { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder dbo)
        {
            dbo.UseNpgsql("Host=localhost;Port=5432;Database=RPS;Username=test_username;Password=123456");
        }
    }
}