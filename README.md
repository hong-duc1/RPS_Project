# Recruitment Process System Project

## Công nghệ sử dụng
+ Dotnet Core
+ EntityFramework Core
+ Postgresql

## Cách build
1. Tải và cài đặt [dotnet core vs2017](https://www.microsoft.com/net/core#windowsvs2017) hoặc [dotnet core cli](https://www.microsoft.com/net/core#windowscmd)
2. Clone repo này về
3. Di chuyển vào thư mục chưa project
4. Vào file DAO/DatabaseContext.cs tìm dòng này `dbo.UseNpgsql("Host=localhost;Port=5432;Database=RPS;Username=test_username;Password=123456");` và thay đổi kết nối theo máy
5. Chạy lệnh `dotnet restore`
4. Mở cmd lên di chuyển vào thư muc project chạy `dotnet ef database update`
5. Mở database bằng pgadmin3 để kiểm tra